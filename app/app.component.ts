import { Component } from '@angular/core';
import { Overlay, overlayConfigFactory } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { LoginModalWindow } from './login.component';
import { OverlayConfig } from 'angular2-modal/esm/models/tokens.d';

@Component({
    selector: 'my-app',
    templateUrl: 'app/tmpl/app.html'
})
export class AppComponent {
    private modalConfig: OverlayConfig;
    constructor(public modal: Modal) { }
    onClickLogin(event): void {
        console.log('login');
        this.modalConfig = overlayConfigFactory({
            dialogClass: 'modal-dialog modal-dialog-login'
        }, BSModalContext);
        this.modal.open(LoginModalWindow, this.modalConfig);
        event.preventDefault();
    }
}
