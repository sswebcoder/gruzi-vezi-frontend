export class User {
    valid: boolean = false;
    name: string;
    surname: string;
    email: string;
    password: string;
    pass_confirm: string;
    birthday: number;
    gender: string = 'male';
    validate(): Object {
        var result: Object = {};
        if (!this.name || !this.name.trim()) {
            result['name'] = 'Поле не может быть пустым';
        }
        if (!this.surname || !this.surname.trim()) {
            result['surname'] = 'Поле не может быть пустым';
        }
        if (!this.email || !this.email.trim()) {
            result['email'] = 'Поле не может быть пустым';
        }
        if (!this.password || !this.password.trim()) {
            result['password'] = 'Поле не может быть пустым';
        }
        if (!this.pass_confirm || !this.pass_confirm.trim()) {
            result['pass_confirm'] = 'Поле не может быть пустым';
        }
        if (!this.birthday) {
            result['birthday'] = 'Выберите год рождения из списка';
        }
        if (Object.keys(result).length) {
            this.valid = false;
        } else {
            this.valid = true;
        }
        return result;
    }
}
