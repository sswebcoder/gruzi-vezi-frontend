import { Component } from '@angular/core';

import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';

export class CustomModalContext extends BSModalContext { }

@Component({
    selector: 'modal-content',
        templateUrl: 'app/tmpl/login-modal.html'
})
export class LoginModalWindow implements CloseGuard, ModalComponent<CustomModalContext> {
    context: CustomModalContext;

    public wrongAnswer: boolean;
    public shouldUseMyClass: boolean;
    public dialogClass: string = 'modal-dialog';

    constructor(public dialog: DialogRef<CustomModalContext>) {
        this.context = dialog.context;
        this.wrongAnswer = true;
        dialog.setCloseGuard(this);
    }

    onKeyUp(value) {
        this.wrongAnswer = value != 5;
        this.dialog.close();
    }


    beforeDismiss(): boolean {
        this.dialog.close();
        return true;
    }

    //beforeClose(): boolean {
        //return this.wrongAnswer;
    //}
    onClickLogin(): void {
        //Temporary just close window.
        this.dialog.close();
    }
}

