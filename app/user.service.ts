import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { User } from './user';

@Injectable()
export class UserService {
    private headers = new Headers({'Content-Type': 'application/json'});
    private url = 'api/users';
    constructor(private http: Http) { }
    create(user: User): Promise<User> {
        return this.http
        .post(this.url, JSON.stringify(user), {headers: this.headers})
        .toPromise()
        .then(res => res.json().data)
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}


