import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';

import { AppComponent } from './app.component';
import { MainPageComponent } from './main-page.component';
import { RegistrationComponent } from './registration.component';
import { UserService }          from './user.service';
import { LoginModalWindow }          from './login.component';
import { CarrierSearchComponent }          from './carrier-search.component';
import { DatePickerModule } from 'ng2-datepicker';
import { NouisliderModule } from 'ng2-nouislider';

@NgModule({
    imports:      [
        BrowserModule,
        RouterModule.forRoot([
            {
                path: '',
                component: MainPageComponent
            }, {
                path: 'registration',
                component: RegistrationComponent
            }, {
                path: 'carrier-search',
                component: CarrierSearchComponent
            }
       ]),
        FormsModule,
        HttpModule,
        ModalModule.forRoot(),
        BootstrapModalModule,
        DatePickerModule,
        NouisliderModule
    ],
    declarations: [
        AppComponent,
        MainPageComponent,
        RegistrationComponent,
        LoginModalWindow,
        CarrierSearchComponent
    ],
    providers: [
        UserService
    ],
    bootstrap:    [ AppComponent ],
    entryComponents: [ LoginModalWindow ]
})
export class AppModule { }

