import { Component, Input } from '@angular/core';
import * as moment from 'moment';


@Component({
    selector: 'carrier-search',
    templateUrl: 'app/tmpl/carrier-search.html',
})
export class CarrierSearchComponent {
    public date: Object;
    public loadTimeRange: number[] = [8,  22];
    public weightRange: number[] = [50,  200];
    public valueRange: number[] = [1,  5];
    loadTimeRangeConfig: any = {
        behaviour: 'drag',
        connect: true,
        margin: 1,
        tooltips: [true, true],
        step: 1,
        range: {
            min: 0,
            max: 24
        },
        pips: {
            mode: 'steps'
        }
    };
    weightRangeConfig: any = {
        behaviour: 'drag',
        connect: true,
        margin: 1,
        tooltips: [true, true],
        step: 1,
        range: {
            min: 0,
            max: 900
        },
        pips: {
            mode: 'steps'
        }
    };
    valueRangeConfig: any = {
        behaviour: 'drag',
        connect: true,
        margin: 1,
        tooltips: [true, true],
        step: 1,
        range: {
            min: 0,
            max: 10
        },
        pips: {
            mode: 'steps'
        }
    };
    constructor() {
        moment.locale('ru');
        this.date = moment();
    }
}


