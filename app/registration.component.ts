import { Component, Input } from '@angular/core';

import { User } from './user';
import { UserService } from './user.service';

let curYear: number = (new Date()).getFullYear();
let years: number[] = [];
for (var i=curYear-70; i < curYear-17; i++) {
    years.push(i);
}
years.reverse();
@Component({
    selector: 'registration',
    templateUrl: 'app/tmpl/registration.html'
})

export class RegistrationComponent {
    user = new User();
    years = years;
    validateErrors: Object;
    constructor(
        private userService: UserService
    ) { }
    register(name: string): boolean {
        console.log('registration');
        this.validateErrors = this.user.validate();
        if (!this.user.valid) {
            return false;
        }
        //TODO: Обработать ответ сервера
        this.userService.create(this.user)
            .then(user => {
                debugger;
            })
            .catch(function() {
                alert('Тут надо обработать ошибку.');
            });
        return false;
    }
    onFieldFocus(fieldName): void {
        delete this.validateErrors[fieldName];
    }
}


