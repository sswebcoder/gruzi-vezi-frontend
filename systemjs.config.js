/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
    System.config({
        paths: {
            // paths serve as alias
            'npm:': 'node_modules/'
        },
        // map tells the System loader where to look for things
        map: {
            // our app is within the app folder
            app: 'app',
            // angular bundles
            '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
            '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
            '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
            '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
            '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
            '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
            '@angular/upgrade': 'npm:@angular/upgrade/bundles/upgrade.umd.js',
            // other libraries
            'rxjs':                      'npm:rxjs',
            'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
            'angular2-modal': 'npm:angular2-modal',
            'angular2-modal/plugins/bootstrap': 'npm:angular2-modal/bundles',
            'ng2-datepicker': 'npm:ng2-datepicker/bundle',
            'moment': 'npm:moment',
            'nouislider': 'npm:nouislider',
            'ng2-nouislider': 'npm:ng2-nouislider',
        },
        // packages tells the System loader how to load when no filename and/or no extension
        packages: {
            app: {
                main: './main.js',
                defaultExtension: 'js'
            },
            rxjs: {
                defaultExtension: 'js'
            },
            'angular2-modal': {
                main: 'bundles/angular2-modal.umd.js',
                defaultExtension: 'js'
            },
            'angular2-modal/plugins/bootstrap': {
                main: 'angular2-modal.bootstrap.umd.js',
                defaultExtension: 'js'
            },
            'ng2-datepicker': {
                main: 'ng2-datepicker.umd.js',
                defaultExtension: 'js',
            },
            'moment': {
                main: 'min/moment-with-locales.js',
                type: 'cjs',
                defaultExtension: 'js'
            },
            'nouislider': {
                main: 'distribute/nouislider.js',
                defaultExtension: 'js'
            },
            'ng2-nouislider': {
                main: 'src/nouislider.js',
                defaultExtension: 'js'
            },
       }
    });
})(this);

